var app = angular.module('wheelOfFortuneApp', []);

app.controller('wheelOfFortuneController',
    ['$scope', function ($scope) {

        $scope.message;
        $scope.players;
        $scope.actualPlayer;
        $scope.showStartButton;
        $scope.showEnterPlayers;
        $scope.slogans;
        $scope.consonants;
        $scope.scores;
        $scope.vowels;
        $scope.round;
        $scope.slogan;
        $scope.buyVowel;
        $scope.game;
        $scope.value;
        $scope.indexSlogan;
        $scope.messageAfterRound;
        $scope.showEndTab;
        $scope.buttonMessage;
        $scope.guessSlogan;
        $scope.description;
        $scope.showScoreValue
        let sloganForCheck;
        let consonantsToGuess;
        let vowelsToGuss;
        let lettersToGuess;

        function initData() {

            $scope.guessSlogan = '';
            $scope.buttonMessage = '';
            $scope.messageAfterRound = '';
            $scope.showEndTab = false;
            $scope.round = 1;
            $scope.message = '';
            $scope.actualPlayer = 0;
            $scope.showStartButton = true;
            $scope.showEnterPlayers = false;
            $scope.value = '0000000000';
            $scope.slogan = '';
            $scope.buyVowel = false;
            $scope.game = false;
            $scope.showScoreValue = '0000000000';
            sloganForCheck = [];
            consonantsToGuess = 0;
            vowelsToGuss = 0;
            lettersToGuess = 0;
            $scope.players = [
                {
                   player: 'Player 1',
                   name: '',
                   score: 0,
                   isActual: true,
                   endScore: 0,
                   nameFun: function(newName) {
                       return arguments.length ? (this.name = newName) : this.name;
                   },
                   actualFun: function(value) {
                       return this.isActual = value;
                   },
                   scoreFun: function(scoreToAdd) {
                       return this.score = this.score + scoreToAdd;
                   },
                   bankruptFun: function() {
                       return this.score = 0;
                   },
                   sumScore: function() {
                        return this.endScore = this.endScore + this.score;
                    },
               },
               {
                   player: 'Player 2',
                   name: '',
                   score: 0,
                   isActual: false,
                   endScore: 0,
                   nameFun: function(newName) {
                       return arguments.length ? (this.name = newName) : this.name;
                   },
                   actualFun: function(value) {
                       return this.isActual = value;
                   },
                   scoreFun: function(scoreToAdd) {
                       return this.score = this.score + scoreToAdd;
                   },
                   bankruptFun: function() {
                       return this.score = 0;
                   },
                   sumScore: function() {
                        return this.endScore = this.endScore + this.score;
                    },
               },
               {
                   player: 'Player 3',
                   name: '',
                   score: 0,
                   isActual: false,
                   endScore: 0,
                   nameFun: function(newName) {
                       return arguments.length ? (this.name = newName) : this.name;
                   },
                   actualFun: function(value) {
                       return this.isActual = value;
                   },
                   scoreFun: function(scoreToAdd) {
                       return this.score = this.score + scoreToAdd;
                   },
                   bankruptFun: function() {
                       return this.score = 0;
                   },
                   sumScore: function() {
                        return this.endScore = this.endScore + this.score;
                    },
               }
           ];

           $scope.slogans = [
            {
                password: 'John Hurt',
                description: 'Actor',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Gary Oldman',
                description: 'Actor',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Brendan Gleeson',
                description: 'Actor',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Nurse',
                description: 'Profession',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Designer',
                description: 'Profession',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Sales representative',
                description: 'Profession',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Skyscraper',
                description: 'Building',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Nursery school',
                description: 'Building',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Museum',
                description: 'Building',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Stuffed cabbage',
                description: 'Dish',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Star Wars',
                description: 'Title',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            },
            {
                password: 'Forrest Gump',
                description: 'Title',
                isused: false,
                setUsed: function() {
                    return this.isused = true;
                },
            }
            ];
        
            $scope.consonants  = [ 
                { letter: 'B', isUsed: false },
                { letter: 'C', isUsed: false },
                { letter: 'D', isUsed: false },
                { letter: 'F', isUsed: false },
                { letter: 'G', isUsed: false },
                { letter: 'H', isUsed: false },
                { letter: 'J', isUsed: false },
                { letter: 'K', isUsed: false },
                { letter: 'M', isUsed: false },
                { letter: 'N', isUsed: false },
                { letter: 'L', isUsed: false },
                { letter: 'P', isUsed: false },
                { letter: 'R', isUsed: false },
                { letter: 'S', isUsed: false },
                { letter: 'T', isUsed: false },
                { letter: 'W', isUsed: false },
                { letter: 'Y', isUsed: false },
                { letter: 'Z', isUsed: false }
            ];
            
            
            $scope.scores  = [ 
                { value: '0'},
                { value: '100'},
                { value: '200'},
                { value: '500'},
                { value: 'Bankrupt'},
                { value: '1200'},
                { value: '1500'},
                { value: '2000'},
                { value: 'Loss'},
                { value: '5000'},
                { value: '1100'},
                { value: '2500'}
            ];
            
            $scope.vowels  = [ 
                { letter: 'A', isUsed: false },
                { letter: 'E', isUsed: false },
                { letter: 'I', isUsed: false },
                { letter: 'O', isUsed: false },
                { letter: 'U', isUsed: false }
            ];
        }

        initData();

        $scope.randomSlogan = () => {
            let isUsed = true;
            let slogan;

            while(isUsed) {
                $scope.indexSlogan = Math.floor(Math.random() * ($scope.slogans.length-0)+0);
                slogan = $scope.slogans[$scope.indexSlogan];
                isUsed = slogan.isused;
            }
            
            $scope.slogans[$scope.indexSlogan].setUsed();
            $scope.description = slogan.description;
            setSlogan(slogan);
        }

        function countLetters(slogan, tab) {
            let counter = 0;
            for ( let i=0; i<slogan.length; i++) {
                for ( let j=0; j<tab.length; j++) {
                    if (slogan[i].toUpperCase() === tab[j].letter.toUpperCase()) {
                        counter++;
                    }
                }
            }
            return counter;
        }

        function counterWhiteSpaces(slogan) {
            let counter = 0;
            for ( let i=0; i<slogan.length; i++) {
                if ( slogan[i] === ' ') {
                    counter++;
                }
            }
            return counter;
        }
        function setSlogan(slogan) {
            $scope.slogan = '';
            sloganForCheck = [];
            const passwordLength = slogan.password.length;
            consonantsToGuess = countLetters(slogan.password, $scope.consonants);
            vowelsToGuss =  countLetters(slogan.password, $scope.vowels)
            lettersToGuess = passwordLength - counterWhiteSpaces(slogan.password);
            $scope.slogan = '<div class="block">';
            for ( let i=0; i<passwordLength+1; i++) {
                if ( slogan.password[i] !== ' ' && passwordLength > i ) {
                    sloganForCheck = sloganForCheck + slogan.password[i];
                    $scope.slogan = $scope.slogan + '<div class="letterSlogan" id="id' + i  + '"></div>';
                } else {
                    sloganForCheck = sloganForCheck + ' ';
                    $scope.slogan = $scope.slogan + '</div><div class="block">';
                }
            }

            removeLastChar();
            $scope.slogan = $scope.slogan + '</div>';
            document.getElementById("border").innerHTML = $scope.slogan;
        }

        function removeLastChar() {
            if(sloganForCheck[sloganForCheck.length-1] === ' ') {
                sloganForCheck = sloganForCheck.slice(0,sloganForCheck.length-1);
            }
        }

        $scope.spinTheWheel = () => {
            if (consonantsToGuess === 0 ) {
                $scope.message = 'There is no more consonants. Now you can buy the vowel or try to guess the slogan';
                $scope.openModal();
            } else {
                if ( $scope.value !== '0000000000' && $scope.value !== 'Bankrupt' && $scope.value !== 'Loss' ) {
                    $scope.message = 'Choose the consonant';
                    $scope.openModal();
                } 
                else {

                    const indexScore = Math.floor(Math.random() * ($scope.scores.length-0)+0);
                    const score = $scope.scores[indexScore];

                    $scope.value = score.value;

                    if (!$('#modalWindow').is(':visible')) {
                        $scope.showScoreValue = $scope.value;
                    }
                    
                    if ($scope.value === 'Bankrupt') {
                        $scope.message = 'Bankrupt!!! You lost all the money. Turne next player';
                        $scope.openModal();
                        $scope.players[$scope.actualPlayer].bankruptFun();
                        $scope.changePlayer();
                        $scope.value = '0000000000';
                    } else if ( $scope.value === 'Loss') {
                        $scope.message = 'Lost the turn. Turne next player';
                        $scope.openModal();
                        $scope.changePlayer();
                        $scope.value = '0000000000';
                    } 
                }
            }
        }

        $scope.start = () => {
            $scope.randomSlogan();
        }

        $scope.startGame = () => {
            if(isNamePlayersSet()) {
                $scope.showEnterPlayers = false;
                $scope.game = true;
                $scope.start();
            } else {
                $scope.message = 'Fill all the name of players';
                $scope.openModal();
            }
         }

        function isNamePlayersSet() {
            isFillName = true;

            for(let i=0; i<$scope.players.length; i++ ) {
                if($scope.players[i].name === '') {
                    isFillName = false;
                }
            }
            return isFillName;
        }

        $scope.enterPalyers = () => {
            $scope.showStartButton = false;
            $scope.showEnterPlayers = true;
        }

        $scope.showVowels = () => {
            if ($scope.value !== '0000000000') {
                $scope.message = 'At first choose the consonant';
                $scope.openModal();
            } else if ($scope.players[$scope.actualPlayer].score < 201) {
                $scope.message = 'You don\'t have enough money. The vowel costs 200';
                $scope.openModal();
            } else {
                $scope.buyVowel = true;
            }
        }

        $scope.checkVowel = (vowel) => {
            $scope.players[$scope.actualPlayer].scoreFun(-200);

            if (vowel.isUsed) {
                $scope.message = 'Letter was used. Turne next player';
                $scope.openModal();
                $scope.changePlayer();
            } else {
                let occurs = 0;
                for( let i=0; i<sloganForCheck.length; i++) {
                    if (vowel.letter === sloganForCheck[i].toUpperCase()) {
                        $scope.slogan[i] = vowel.letter;
                        let id = 'id' + i;
                        document.getElementById(id).innerHTML = vowel.letter;
                        occurs++;
                    }
                }

                if ( occurs > 0 ) {
                    vowel.isUsed = true;
                    vowelsToGuss = vowelsToGuss = occurs;
                    lettersToGuess = lettersToGuess - occurs;
                    $scope.message = 'You bought ' + vowel.letter + '. Letter is ' + occurs + ' times !!!';
                    $scope.openModal();
                } else {
                    $scope.message = 'Letter is no in slogan. Turne next player';
                    $scope.openModal();
                    $scope.changePlayer();
                }          
            }   
            $scope.buyVowel = false;
            isEndOfRoundOrGame();
        }

        $scope.checkConsonant = (consonant) => {
            
            if ( $scope.value === '0000000000' ) {
                $scope.message = 'Spin the wheel first';
                $scope.openModal();
            } else {

                if (consonant.isUsed) {
                    $scope.message = 'Letter was used. Turne next player';
                    $scope.openModal();
                    $scope.changePlayer();
                } else {
                    let occurs = 0;
                    for( let i=0; i<sloganForCheck.length; i++) {
                        if (consonant.letter === sloganForCheck[i].toUpperCase()) {
                            $scope.slogan[i] = consonant.letter;
                            let id = 'id' + i;
                            document.getElementById(id).innerHTML = consonant.letter;
                            occurs++;
                        }
                    }
                    if ( occurs > 0 ) {
                        let value = $scope.value;
                        let score = value * occurs;
                        consonantsToGuess = consonantsToGuess - occurs;
                        lettersToGuess = lettersToGuess - occurs;
                        $scope.players[$scope.actualPlayer].scoreFun(score);
                        $scope.message = 'Letter is ' + occurs + ' times in slogan. You win ' + score + '!!!';        
                    } else {
                        $scope.message = 'Letter is no in slogan. Turne next player';
                        $scope.changePlayer();
                    }
                    $scope.openModal();
                    $scope.value = '0000000000';
                    consonant.isUsed = true;
                }
                isEndOfRoundOrGame();
            }
        }

        $scope.changePlayer = () => {
            
            if($scope.actualPlayer === 2) {
                $scope.actualPlayer = 0;
            } else {
                $scope.actualPlayer++;
            }

            for ( let i =0; i<3; i++) {
                if ( i === $scope.actualPlayer) {
                    $scope.players[i].actualFun(true);
                } else {
                    $scope.players[i].actualFun(false);
                }
            }
            $scope.value = '0000000000';
        }
        
        $scope.openModal = () => {
            $('#modalWindow').modal({
                backdrop: 'static',
                keyboard: false
            });
            
        }

        $scope.closeModal = () => {
            $('#modalWindow').modal('hide');
                $scope.showScoreValue = $scope.value;
        }

        $scope.tryGuess = () => {
            if ($scope.value !== '0000000000') {
                $scope.message = 'At first choose the consonant';
                $scope.openModal();
            } else {
                $('#modalWindowTryGuess').modal();
            }
        }

        $scope.resetDataForNextRound = () => {
            if($scope.round === 3) {
                initData();
            } else {
                $scope.buttonMessage = '';
                $scope.messageAfterRound = '';
                $scope.showEndTab = false;
                $scope.message = '';
                $scope.message2 = '';
                $scope.actualPlayer = 0;
                $scope.value = '0000000000';
                $scope.guessSlogan = '';
                $scope.round = $scope.round + 1;

                for ( let i=0; i<$scope.players.length; i++) {
                    $scope.players[i].bankruptFun();
                    $scope.players[i].actualFun(false);
                }
                $scope.players[$scope.actualPlayer].actualFun(true);

                for ( let i=0; i<$scope.consonants.length; i++) {
                    $scope.consonants[i].isUsed = false;
                }

                for ( let i=0; i<$scope.vowels.length; i++) {
                    $scope.vowels[i].isUsed = false;
                }
                $scope.randomSlogan();
            }
            $scope.closeModal();
        }

        function setDataForEndTab() {
            $scope.message = 'The ' + $scope.round + ' win player: ' + $scope.players[$scope.actualPlayer].name + ' with score: ' + $scope.players[$scope.actualPlayer].score;
            $scope.players[$scope.actualPlayer].sumScore();
            $scope.showEndTab = true;
            $scope.openModal();
            $scope.message = '';
        }

        function isEndOfRoundOrGame() {

            if(isEndOfRound()) {
                if($scope.round === 3) {
                    $scope.messageAfterRound = 'Table after last (3) round. The winner is ' + $scope.players[$scope.actualPlayer].name;
                    $scope.buttonMessage = 'Play again';
                    setDataForEndTab();
                } else {
                    $scope.messageAfterRound = 'Table after ' + $scope.round + ' round';
                    setDataForEndTab();
                    $scope.buttonMessage = 'Next round';
                }
            }
        }

        $scope.checkSloga = () => {
            if($scope.guessSlogan !== '') {
                if ($scope.guessSlogan.toUpperCase() === sloganForCheck.toUpperCase()) {
                    if($scope.round === 3) {
                        $scope.message2 = 'Correct slogan !!!';
                        $scope.messageAfterRound = 'Table after last (3) round. The winner is ' + $scope.players[$scope.actualPlayer].name;
                        $scope.buttonMessage = 'Play again';
                        setDataForEndTab();
                    } else {
                        $scope.message2 = 'Correct slogan !!!';
                        $scope.messageAfterRound = 'Table after ' + $scope.round + ' round';
                        setDataForEndTab();
                        $scope.buttonMessage = 'Next round';
                    }
                } else if ($scope.guessSlogan !== '') {
                    $scope.message = 'incorrect slogan !!! Turen the next player';
                    $scope.changePlayer();
                    $scope.openModal();
                    $scope.message2= '';
                }
            }
        }

        function isEndOfRound() {
            return lettersToGuess > 0 ? false : true;
        }
    }]
);